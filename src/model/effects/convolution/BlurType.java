package model.effects.convolution;

/**
 * Represents all the blur types .
 */
public enum BlurType {
    /**
     * type of blur .
     */
    FAST_BLUR, GAUSSIAN_BLUR, BOX_BLUR
}
