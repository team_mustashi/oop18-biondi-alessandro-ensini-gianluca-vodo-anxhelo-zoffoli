package view.util.theme;

/**
 * Represents all the available theme .
 */
public enum Themes {
    /**
     * css theme .
     */
    JMetroDarkTheme, JMetroLightTheme, ModenaDark, ModenaLight
}
