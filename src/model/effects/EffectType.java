package model.effects;

/**
 * Enum that identify the type of each effect.
 */
public enum EffectType {
    /**
     * Void type effect.
     */
    Void,
    /**
     * Convolution type effect.
     */
    Convolution,
    /**
     * Filter type effect.
     */
    Filter,
    /**
     * Regolation type effect.
     */
    Regolation,
    /**
     * Tool type effect.
     */
    Tool
}
